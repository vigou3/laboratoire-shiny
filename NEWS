===
=== Rapports dynamiques avec Shiny
===

# 2024.03 (2024-03-16)

## Changements

- Présentation générale: le nom de la compagnie derrière Shiny n'est
  plus RStudio, mais bien Posit.
- Tous les liens vers rstudio.com ont été mis à jour pour posit.co.
- Petites améliorations au texte, notamment à la présentation des
  expressions réactives.
- Images redimensionnées pour améliorer le visuel des diapositives.


# 2023.01 (2023-01-05)

## Changements

- Modification du nom du projet.
- Mathématiques en Fira Math.


# 2022.03 (2022-03-20)

## Changements

- Élimination du niveau de section «Construction d'une application».
- Ajout d'une nouvelle section «Création dynamique de contenu».
- Dernières remarques: francisation d'une expression.


# 2021.03 (2021-03-19)

## Changements

- Suppression des liens vers des vidéos explicatives dans YouTube. Les
  vidéos sont plutôt disponibles depuis le site de cours.
- Changements cosmétiques mineurs.


# 2020.03 (2020-03-20)

## Nouveautés

Dans le cadre de la quarantaine de la COVID-19, j'ai préparé une série
de 5 vidéos de présentation de la formation. Cette version intègre les
liens vers les vidéos dans YouTube.


# 2020.02 (2020-02-05)

## Nouveautés

- Table des matières pour les diapositives.
- Page web pour le projet.
- Utilisation de l'infrastructure de *release* de GitLab pour la
  distribution des versions.

## Changements

- Utilisation d'une icône dans l'entête pour identifier les
  diapositives d'exercices.
- Suppression des intertitres qui servaient à identifier les
  exercices.


# 2019.04 (2019-04-07)

## Nouveautés

- Précisions additionnelles dans la diapositive sur la publication de
  ses applications Shiny.

## Autres modifications

- Code informatique présenté dans une couleur contrastante.


# 2019.03-1 (2019-03-29)

## Nouveautés

- Accent plus fort sur le fait que les expressions réactives sont
  aussi utiles lorsque l'on doit utiliser des résultats plus d'une
  fois dans `server`. Ajout d'une puce à cet effet à la diapositive 24
  et de deux astuces à la diapositive 27.

## Autres modifications

- Correction à la notice de copyright de la photo de la couverture.
- Modification de la présentation sur le chargement de MathJaX.
- Mention explicite que les mathématiques utilisent la syntaxe LaTeX.


# 2019.03 (2019-03-29)

## Nouveautés

- Les diapositives se présentent maintenant sous forme de document
  PDF. Il n'y a plus de version en ligne des diapositives dans GitLab.

## Autres modifications

- Suppression de l'exercice sur le texte et les mathématiques et, par
  conséquent, de l'application `exercice-texte-math`.


# 2019.02 (2019-02-06)

## Nouveautés

- Ajout d'un avertissement sur l'utilisation des sources et
  expressions réactives.

## Autres modifications

- Suite au déplacement du projet vers GitLab: modification de divers
  liens dans les diapositives et réorganisation des fichiers du code
  source.


# 2018.02a (2018-02-23)

Correction de coquilles et d'omissions dans les diapos, amélioration
du code des applications.

- Équations de l'Expected Shortfall manquantes sur la diapo de
  présentation du projet.
- Meilleurs exemples de syntaxe dans la section «Entrée et sortie».
- Fonction `parse` remplacée par `paste` dans l'exemple de syntaxe des
  fonctions `render*`.
- Nouvelle diapositive sur la publication de ses applications Shiny.
- Application `exercice-widgets` (solution): utilisation de noms
  différents pour les éléments de `input` et de `output` pour éviter
  la possible confusion (chez le lecteur, pas pour R).
- Applications `shortfall`, `exercice-reactive` et
  `exercice-texte-math`: françisation de l'affichage des paramètres
  (espace dans les nombres en milliers, virgule comme séparateur
  décimal).

# 2018.02 (2018-02-23)

Version initiale.
